import data from './properties.json'

// ------------------------------------
// Constants
// ------------------------------
export const TOGGLE_SELECT = 'toggle_select'
// ------------------------------------
// Actions
// ------------------------------------

export const properties = data.map((property, index) => {
  property.id = index
  property.value = (property.price / property.floor_area).toFixed(2)
  property.selected = false
  return property
})

export function toggleSelect (id) {
  return {
    type : TOGGLE_SELECT,
    payload : id
  }
}

export const actions = {
  toggleSelect
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [TOGGLE_SELECT]: (state, action) => {
    const propertyId = action.payload
    return Object.assign({}, state, {
      properties: state.properties.map((property, index) => {
        if (index === propertyId) {
          return Object.assign({}, property, {
            selected: !property.selected
          })
        }
        return property
      })
    })
  }
}
// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  properties: properties
}
export default function calculatorReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
