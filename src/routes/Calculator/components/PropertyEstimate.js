import React from 'react'
import { connect } from 'react-redux'

export const PropertyEstimate = (props) => (
  <div className='estimate'>
    <h3>Area Estimate</h3>
    <div><span className='value'>£{props.estimate}</span> per sq. ft.</div>
    <div><span className='totalValue'>£{(props.estimate * 1100).toFixed(2)}</span> for a 1,100 sq. ft. property</div>
  </div>
)

PropertyEstimate.propTypes = {
  estimate: React.PropTypes.number.isRequired
}

const mapStateToProps = (state) => {
  let valueSum = state.calculator.properties
    .filter((property) => property.selected)
    .map((property) => property.value);
  return {
    estimate : valueSum.length ?
      valueSum.reduce((a, b) => parseInt(a) + parseInt(b), 0) / valueSum.length :
      0
  }
}

export default connect(mapStateToProps)(PropertyEstimate)
