import React from 'react'
import { connect } from 'react-redux'
import * as actions from '../modules'

export const PropertyEntry = (props) => {
  let property = props.property
  return (
    <tr className={property.selected ? 'selected' : ''}>
      <td>
        <input
          type='checkbox'
          checked={property.selected}
          onChange={() => props.toggleSelect(property.id)}
        />
      </td>
      <td>{property.address}</td>
      <td>£{property.price}</td>
      <td>{property.floor_area} sq.ft</td>
      <td>£{property.value}</td>
    </tr>
  )
}

PropertyEntry.propTypes = {
  id: React.PropTypes.number.isRequired,
  property: React.PropTypes.object.isRequired,
  toggleSelect: React.PropTypes.func.isRequired
}

const mapStateToProps = (state, ownProps) => {
  return {
    property : state.calculator.properties[ownProps.id]
  }
}

export default connect(mapStateToProps, actions)(PropertyEntry)
