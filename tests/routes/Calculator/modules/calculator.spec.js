import {
  TOGGLE_SELECT,
  toggleSelect,
  properties,
  default as calculatorReducer
} from 'routes/Calculator/modules'

describe('(Redux Module) Calculator', () => {
  it('Should export a constant TOGGLE_SELECT.', () => {
    expect(TOGGLE_SELECT).to.equal('toggle_select')
  })

  describe('(Reducer)', () => {
    it('Should be a function.', () => {
      expect(calculatorReducer).to.be.a('function')
    })

    it('Should initialize with a properties array in state.', () => {
      expect(calculatorReducer(undefined, {}).properties).to.be.an.array
    })
    it('Should initialize with a properties array of length 5', () => {
      expect(calculatorReducer(undefined, {}).properties).to.have.length(5)
    })
    it('Should initialize with the properties data', () => {
      expect(calculatorReducer(undefined, {}).properties).to.equal(properties)
    })

    it('Should return the previous state if an action was not matched.', () => {
      let state = calculatorReducer(undefined, {})
      expect(state.properties).to.be.an.array
      expect(state.properties).to.have.length(5)
      state = calculatorReducer(state, { type: '@@@@@@@' })
      expect(state.properties).to.be.an.array
      expect(state.properties).to.have.length(5)
      // state = calculatorReducer(state, increment(5))
      // expect(state).to.equal(5)
      // state = calculatorReducer(state, { type: '@@@@@@@' })
      // expect(state).to.equal(5)
    })
  })

  describe('(Action Creator) toggleSelect', () => {
    it('Should be exported as a function.', () => {
      expect(toggleSelect).to.be.a('function')
    })

    it('Should return an action with type "toggle_select".', () => {
      expect(toggleSelect(0)).to.have.property('type', TOGGLE_SELECT)
    })

    it('Should assign the first argument to the "payload" property.', () => {
      expect(toggleSelect(0)).to.have.property('payload', 0)
    })
  })

  // NOTE: if you have a more complex state, you will probably want to verify
  // that you did not mutate the state. In this case our state is just a number
  // (which cannot be mutated).
  describe('Reducer toggle_selct', () => {
    describe('Update the state properties by the action payload\'s "value" property.', () => {
      let state = calculatorReducer(undefined, {})

      it('Should return initial state', () => {
        expect(state.properties).to.equal(properties)
      })
      it('Should return new state', () => {
        state = calculatorReducer(state, toggleSelect(1))
        expect(state.properties[1].selected).to.be.true
        state = calculatorReducer(state, toggleSelect(2))
        expect(state.properties[2].selected).to.be.true
        state = calculatorReducer(state, toggleSelect(2))
        expect(state.properties[2].selected).to.be.false
      })
    })
  })
})
