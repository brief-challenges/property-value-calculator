import React from 'react'
import { HomeView } from 'routes/Home/components/HomeView'
import { render } from 'enzyme'

describe('(View) Home', () => {
  let _component

  beforeEach(() => {
    _component = render(<HomeView />)
  })

  it('Renders an awesome profile image', () => {
    const profileImg = _component.find('img')
    expect(profileImg).to.exist
    expect(profileImg.attr('alt')).to.match(/Tania Papazafeiropoulou/)
  })
})
