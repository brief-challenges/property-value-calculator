import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path: 'calculator',
  getComponent (nextState, next) {
    require.ensure([], (require) => {
      const Calculator = require('./components/Calculator').default
      const calculatorReducer = require('./modules').default

      injectReducer(store, {
        key: 'calculator',
        reducer: calculatorReducer
      })

      next(null, Calculator)
    })
  }
})
