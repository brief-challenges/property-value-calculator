import React from 'react'
import ProfileImg from '../assets/profile.svg'
import './HomeView.scss'

export const HomeView = () => (
  <div>
    <img
      alt='Tania Papazafeiropoulou'
      className='duck'
      src={ProfileImg} />
  </div>
)

export default HomeView
