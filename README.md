# Area value estimate

This is a simple web application using React and Redux. It allows a user to interactively get an estimate for a property value, by selecting which known properties will be included in the calculation.

#### Description
One way of valuing property is to take a number of comparable properties in the same area which have sold recently, calculate a price per square foot of floor area for each one and use the average value multiplied by the floor area of the property being valued. For example, with the following three comparable properties:

---

Property | Price | Floor Area | Price Per Sq. Ft.
--- | --- | --- | ---
1 | £300,000 | 1,000 | £300
2 | £534,000 | 1,200 | £445
3 | £460,000 | 1,000 | £460

---

Average price per sq. ft. (£300 + £445 + £480) / 3 = £408.

Given this data, for a 980 sq. ft. property the estimated value would be £408 × 980 = £399,840. Although we have data to do a good job of roughly guessing which properties are comparable, the user typically knows their area better than we do, so it makes sense to allow them to edit the list.
