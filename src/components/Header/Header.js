import React from 'react'
import { IndexLink, Link } from 'react-router'
import './Header.scss'

export const Header = () => (
  <div>
    <h1>Welcome</h1>
    <IndexLink to='/' activeClassName='route--active'>
      Home
    </IndexLink>
    {' · '}
    <Link to='/calculator' activeClassName='route--active'>
      Area Value Calculator
    </Link>
    {' · '}
    <a href='http://tany4.com'>
      tany4.com
    </a>
  </div>
)

export default Header
