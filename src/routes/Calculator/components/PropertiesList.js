import React from 'react'
import { connect } from 'react-redux'
import PropertyEntry from './PropertyEntry'

// Use named export for unconnected component (for tests)
export const PropertiesList = (props) => (
  <table className='table table-bordered'>
    <thead>
      <tr>
        <th>Select:</th>
        <th>Property Address</th>
        <th>Value</th>
        <th>Floor Area</th>
        <th>Value per sq.ft.</th>
      </tr>
    </thead>
    <tbody>
      {props.properties.map((property, index) => {
        property.id = index
        return (
          <PropertyEntry key={index} id={index} />
        )
      })}
    </tbody>
  </table>
)

PropertiesList.propTypes = {
  properties: React.PropTypes.array.isRequired
}
const mapStateToProps = (state, ownProps) => {
  return {
    properties : state.calculator.properties
  }
}

export default connect(mapStateToProps)(PropertiesList)
