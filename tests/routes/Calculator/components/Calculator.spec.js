import React from 'react'
import { bindActionCreators } from 'redux'
import { Calculator } from 'routes/Calculator/components/Calculator'
import { PropertyEstimate } from 'routes/Calculator/components/PropertyEstimate'
import { PropertiesList } from 'routes/Calculator/components/PropertiesList'
import { shallow } from 'enzyme'
import testProperties from '../modules/test_properties.json'

describe('(Component) Calculator', () => {
  let _props,
    _spies,
    _wrapper,
    _propertiesList,
    _estimate

  beforeEach(() => {
    _spies = {}
    _props = {
      properties: testProperties,
      estimate: 0,
      ...bindActionCreators({
        toggleSelect: (_spies.toggleSelect = sinon.spy())
      }, _spies.dispatch = sinon.spy())
    }
    _wrapper = shallow(<Calculator {..._props} />)
    _propertiesList = shallow(<PropertiesList properties={_props.properties} />)
    _estimate = shallow(<PropertyEstimate estimate={_props.estimate} />)
  })

  it('Calculator should render as a <div>.', () => {
    expect(_wrapper.is('div')).to.equal(true)
  })

  it('Should render an .estimate div', () => {
    expect(_estimate.is('div')).to.equal(true)
    expect(_estimate.hasClass('estimate')).to.be.true
    expect(_estimate.find('.value').text()).to.match(/£0/)
    expect(_estimate.find('.totalValue').text()).to.match(/£0/)
  })

  it('Should render with .table div', () => {
    expect(_propertiesList.is('table')).to.equal(true)
    expect(_propertiesList.hasClass('table')).to.be.true
  })
})
