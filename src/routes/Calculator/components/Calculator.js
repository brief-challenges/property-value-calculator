import React from 'react'
// import { connect } from 'react-redux'
// import { toggleSelect } from '../modules/'

import './Calculator.scss'
import PropertiesList from './PropertiesList'
import PropertyEstimate from './PropertyEstimate'

export const Calculator = (props) => {
  // console.log(props)
  return (
    <div >
      <PropertiesList />
      <PropertyEstimate />
    </div>
  )
}

// Calculator.propTypes = {
//   properties: React.PropTypes.array.isRequired,
//   toggleSelect: React.PropTypes.func.isRequired
// }
//
// const mapDispatchToProps = {
//   toggleSelect : (id) => toggleSelect
// }
//
// const mapStateToProps = (state) => {
//   console.log(state)
//   return {
//     properties : state.calculator.properties,
//     selected: state.calculator.selected
//   }
// }

export default Calculator
