import CalculatorRoute from 'routes/Calculator'

describe('(Route) Calculator', () => {
  let _route

  beforeEach(() => {
    _route = CalculatorRoute({})
  })

  it('Should return a route configuration object', () => {
    expect(typeof _route).to.equal('object')
  })

  it('Configuration should contain path `calculator`', () => {
    expect(_route.path).to.equal('calculator')
  })
})
